export const environment = {
  production: true,
  fichaTramiteUrlBase: "https://beta.www.gov.co/",
  serverUrl: 'https://preapi-interno.www.gov.co/api/tramites-y-servicios/'
};
