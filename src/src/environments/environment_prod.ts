export const environment = {
  production: true,
  fichaTramiteUrlBase: "https://beta.www.gov.co/",
  serverUrl: "https://tramiteswa.www.gov.co/api/"
};
