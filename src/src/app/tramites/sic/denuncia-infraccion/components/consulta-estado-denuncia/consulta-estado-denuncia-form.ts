import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export class ConsultaEstadoDenunciaForm {
  public form: FormGroup;
  private formBuilder: any;

  constructor() {
    this.formBuilder = new FormBuilder();
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      anyo: ['', // Validators.required
      ],
      numero_radicado: ['', [
        Validators.minLength(4),
        Validators.maxLength(10),
        // Validators.required,
        Validators.pattern('^[0-9]+$')]
      ],
      numeroDocumento: ['',[
        Validators.minLength(8),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]+$')]
        // Validators.required
      ],
      tipoDocumento: ['',
        // Validators.required
      ],
    }, { validator: this.customValidationFunction });
  }

  customValidationFunction(formGroup): any {
    const anioField = formGroup.controls['anyo'].value;
    const noRadicadoField = formGroup.controls['numero_radicado'].value;
    const noDocumentoField = formGroup.controls['numeroDocumento'].value;
    const tipoDocumentoField = formGroup.controls['tipoDocumento'].value;
    const seccion1 = Object.keys(anioField).length > 0 && noRadicadoField.length > 0;
    const seccion2 = noDocumentoField.length > 0 && Object.keys(tipoDocumentoField).length > 0;
    const error = seccion1 || seccion2;
    return (!error) ? { atLeastOne: true } : null;
  }

  getForm(): FormGroup {
    return this.form;
  }

  getFormValues() {
    return this.form.value;
  }

  isValid() {
    return this.form.valid;
  }
}
