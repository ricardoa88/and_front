import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ConsultaEstadoDenunciaForm } from './consulta-estado-denuncia-form';
import { SicUtilsService } from '../../services/sic-utils.service';
import { ConsultaRadicacion } from '../../models/sic-models';
import { GovcoTableModel, TableCellModel } from '@shared/forms/models/table.model';

@Component({
  selector: 'app-consulta-estado-denuncia',
  templateUrl: './consulta-estado-denuncia.component.html',
  styleUrls: ['./consulta-estado-denuncia.component.scss']
})
export class ConsultaEstadoDenunciaComponent implements OnInit {
  public tDetail: GovcoTableModel;

  consultaRadicacion: ConsultaRadicacion;
  seleccionForm: FormGroup;
  seleccionSolucionForm: ConsultaEstadoDenunciaForm;
  invalidForm: boolean;
  listaAnyo: any = [];
  listaTipoDocumento: any = [];
  results: any[] = [];
  numero: any;
  control: any;
  url: any;
  idsTablaSedes: string[] = [
    'anio',
    'numero',
    'control',
    'consecutivo',
    'secuenciaEvento',
    'perfil',
    'tipoRadicacion',
    'fechaRadicacion',
    'solicitante',
    'urlDocumento',
  ];
  idsDetalleTablaSedes: string[] = [
    'nombreTramite',
    'nombreEvento',
    'nombreActuacion'
  ];

  private show: boolean;

  constructor(private sicUtilsService: SicUtilsService) {
    this.setTablaDetalleDatos();
    this.cargarListasGenericas();
  }

  cargarListasGenericas() {
    // Tipo de documento
    this.sicUtilsService.getListaGenericas('TIPO_DOCUMENTO_PERSONA')
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaTipoDocumento = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  ngOnInit() {
    this.seleccionSolucionForm = new ConsultaEstadoDenunciaForm();
    this.buildForm();
    this.llenarAnyo();
    this.inicializarTablaDetalleSedes();
    this.setTablaDetalleDatos();

  }
  public inicializarTablaDetalleSedes() {
    this.tDetail = {
      Header: [
        { content: 'Radicado', filter: false, typeFilter: '' },
        { content: 'Año', filter: true, typeFilter: '' },
        { content: 'Número', filter: false, typeFilter: '' },
        { content: 'Control', filter: false, typeFilter: '' },
        { content: 'Consecutivo', filter: false, typeFilter: '' },
        { content: 'Secuencia', filter: false, typeFilter: '' },
        { content: 'Trámite', filter: false, typeFilter: '' },
        { content: 'Evento', filter: false, typeFilter: '' },
        { content: 'Actuación', filter: false, typeFilter: '' },
        { content: 'Tipo radicacion', filter: false, typeFilter: '' },
        { content: 'Fecha', filter: false, typeFilter: '' },
        { content: 'Solicitante', filter: false, typeFilter: '' },
        { content: 'Adjunto', filter: false, typeFilter: '' }
      ],
      Body: [],
      ConfigHeader: [],
      ConfigBody: []
    };
  }

  consultar() {
    if (this.seleccionSolucionForm.isValid()) {
      this.consultaRadicacion = {
        anio: Object.keys(this.seleccionForm.value.anyo).length === 0
          // tslint:disable-next-line: radix
          ? null : Number.parseInt(this.seleccionForm.value.anyo.value),
        // tslint:disable-next-line: radix
        numero: this.seleccionForm.value.numero_radicado.length === 0
          ? null : Number.parseFloat(this.seleccionForm.value.numero_radicado),
        numeroDocumento: this.seleccionForm.value.numeroDocumento.length === 0
          // tslint:disable-next-line: radix
          ? null : Number.parseInt(this.seleccionForm.value.numeroDocumento),
        tipoDocumento: Object.keys(this.seleccionForm.value.tipoDocumento).length === 0
          ? null : this.seleccionForm.value.tipoDocumento.value
      };
      this.sicUtilsService.getRadicado(this.consultaRadicacion).subscribe(
        response => {
          if (response) {
            this.show = true;
            this.results = response.expedientes;
            sessionStorage.setItem('dataSimple', JSON.stringify(this.results));
            this.inicializarTablaDetalleSedes();
            this.setTablaDetalleDatos();
          } else {
            console.error('No responde el servicio');
          }
        },
        error => {
          console.error(error);
        },
      );
    } else {
      this.invalidForm = true;
      return;
    }
  }

  public setTablaDetalleDatos() {
    // tslint:disable-next-line: prefer-for-of
    for (let indexDatos = 0; indexDatos < this.results.length; indexDatos++) {
      const listElements = [];
      const data = Object.keys(this.results[indexDatos]);
      sessionStorage.setItem('data', JSON.stringify(data));
      const noRadicado: TableCellModel = {
        content: this.results[indexDatos].anio + '' + this.results[indexDatos].numero
        + '' + this.results[indexDatos].control  + '' + this.results[indexDatos].consecutivo
      };
      listElements.push(noRadicado);
      // tslint:disable-next-line: prefer-for-of
      for (let indexIds = 0; indexIds < this.idsTablaSedes.length; indexIds++) {
        // Se recorre la fila, para obtener el orden especifico que se requiere
        // tslint:disable-next-line: prefer-for-of
        for (let j = 0; j < data.length; j++) {
          const element = this.results[indexDatos][data[j]];
          const el: TableCellModel = {
            content: element
          };

          if (data[j] === this.idsTablaSedes[indexIds]) {
            if (this.idsTablaSedes[indexIds] === 'perfil') {
              // tslint:disable-next-line: forin
              for (const property in this.results[indexDatos].perfil) {
                this.idsDetalleTablaSedes.forEach(elementDetail => {
                  if (elementDetail === property) {
                    const innerElement: TableCellModel = {
                      content: this.results[indexDatos].perfil[property]
                    };
                    listElements.push(innerElement);
                  }
                });
              }
            } else {
              if (this.idsTablaSedes[indexIds] === 'urlDocumento') {
                const customLink: TableCellModel = {
                  content: '',
                  class: 'govco-icon govco-icon-down-arrow-n btn btn-size-1 btn-transparent color-dodger-blue',
                  type: 'button'
                };
                customLink.event = (event) => {
                  if (event.type === 'click') {
                    const win = window.open(element, '_blank');
                    win.focus();
                  }
                };
                listElements.push(customLink);
              } else {
                listElements.push(el);
              }
            }
          }
        }
      }
      // Se adiciona la fila, con el orden de campos especificado
      this.tDetail.Body.push(listElements);
      sessionStorage.setItem('data2', JSON.stringify(listElements));
    }
  }

  llenarAnyo() {
    for (let i = 21; i >= 18; i--) {
      this.listaAnyo.push({ text: i.toString(), value: i.toString() });
    }
  }

  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }
  nueva_consulta() {
    this.seleccionForm.controls.anyo.setValue('');
    this.seleccionForm.controls.numero_radicado.setValue('');
    this.seleccionForm.controls.tipoDocumento.setValue('');
    this.seleccionForm.controls.numeroDocumento.setValue('');
    this.sicUtilsService.setdesplegarGrilla = true;
  }
}
