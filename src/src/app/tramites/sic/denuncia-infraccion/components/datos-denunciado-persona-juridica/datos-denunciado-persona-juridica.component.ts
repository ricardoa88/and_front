import { Component, OnInit } from '@angular/core';
import { DatosDenunciadoPersonaJuridicaForm } from './datos-denunciado-persona-juridica-form';
import { Router } from '@angular/router';
import { FormGroup, Validators } from '@angular/forms';
import { SicUtilsService } from '../../services/sic-utils.service';
import {
  Persona,
  Natural,
  DireccionesEntity,
  EmailsEntity,
  TelefonosEntity,
  responseService,
  Empresa,
  DenunciadoEmpresa,
  Denunciado,
  RepresentanteDenunciado,
  requestUsuarioxDocumento
} from '../../models/sic-models';

@Component({
  selector: 'app-datos-denunciado-persona-juridica',
  templateUrl: './datos-denunciado-persona-juridica.component.html',
  styleUrls: ['./datos-denunciado-persona-juridica.component.scss']
})
export class DatosDenunciadoPersonaJuridicaComponent implements OnInit {
  responsePersona: responseService;
  responsePersona2: responseService;
  requestUsuarioxDocumento: requestUsuarioxDocumento;
  persona: Persona;
  DatosOcultar: boolean;
  proteccionDatos = false;
  seleccionForm: FormGroup;
  seleccionSolucionForm: DatosDenunciadoPersonaJuridicaForm;
  invalidForm = false;
  listaTipoDocumento: any = [];
  listaPais: any = [];
  listaDepartamento: any = [];
  listaCiudad: any = [];
  natural: Natural;
  empresa: Empresa;
  direcciones: DireccionesEntity[];
  mail: EmailsEntity[];
  mailRepresentante: EmailsEntity[];
  telefono: TelefonosEntity[];
  denunciado: Denunciado;
  denunciadoEmpresa: DenunciadoEmpresa;
  representanteLegal: RepresentanteDenunciado;

  constructor(private router: Router, private sicUtils: SicUtilsService) { }

  ngOnInit() {
    this.seleccionSolucionForm = new DatosDenunciadoPersonaJuridicaForm();
    this.buildForm();
    this.cargarListasGenericas();
  }

  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }

  accion_continuar() {
    if (this.seleccionSolucionForm.isValid()) {
      this.registrarPersonaJuridica();
    } else {
      this.invalidForm = true;
      return;
    }
  }
  siguiente() {
    if (this.seleccionSolucionForm.isValid()) {
      if (this.responsePersona.persona == null) {
        this.proteccionDatos = true;
        this.registrarPersonaJuridica();

      } else {
        this.registrarPersonaJuridicaExistente();

      }
    } else {
      this.invalidForm = true;
      return;
    }
  }
  accion_anterior() {
    this.router.navigate(['/sic/datos_denunciante']);
  }
  limpiar() {
    this.invalidForm = false;
    this.seleccionForm.reset();
    this.seleccionForm.enable();
    this.proteccionDatos = false;
  }
  setValidator() {
    // tslint:disable-next-line:triple-equals
    if (this.seleccionForm.value.tipo_documento.value == 'PA') {
      this.seleccionForm.get('numero_documento').setValidators([
        Validators.required,
        Validators.minLength(4), Validators.maxLength(12),
        Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]);
      this.seleccionForm.get('numero_documento').updateValueAndValidity();
    } else {
      this.seleccionForm.controls.numero_documento.setValidators([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12),
        Validators.pattern('^[0-9]+$')
    ]);
      this.seleccionForm.controls.numero_documento.updateValueAndValidity();
    }
  }

  cargarListaDepartamento() {
    this.seleccionForm.controls.departamento.reset();
    this.sicUtils.getListaRegion(this.seleccionForm.value.pais.value)
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaDepartamento = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  cargarListaCiudad() {
    this.seleccionForm.controls.ciudad.reset();
    this.sicUtils.getListaCiudad(this.seleccionForm.value.departamento.value)
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaCiudad = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  cargarListasGenericas() {
    // Tipo de documento
    this.sicUtils.getListaGenericas('TIPO_DOCUMENTO_PERSONA')
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaTipoDocumento = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
    // Pais
    this.sicUtils.getListaGenericas('PAIS')
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaPais = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  registrarPersonaJuridica() {
    this.denunciadoEmpresa = {
      descripcion: '',
      digitoVerificacion: '',
      razonsocial:  this.seleccionForm.value.razon_social,
    };
    this.mail = [{
      tipo: 'PE',
      descripcion: this.seleccionForm.value.correo
    }];
    this.mailRepresentante = [{
      tipo: 'PE',
      descripcion: this.seleccionForm.value.correo_apoderado
    }];
    this.telefono = [{
      tipo: 'CE',
      numero: this.seleccionForm.value.celular
    },
      {
        tipo: 'FI',
        numero: this.seleccionForm.value.telefono_fijo
      },
      {
        tipo: 'FX',
        numero: this.seleccionForm.value.fax
      }
    ];
    this.direcciones = [{
      codigoPais: this.seleccionForm.value.pais.value,
      tipo: 'PE',
      descripcion: this.seleccionForm.value.direccion,
      codigoCiudad: this.seleccionForm.value.ciudad.value,
      codigoRegion: this.seleccionForm.value.departamento.value,
      telefonos: this.telefono
    }];
    this.natural = {
      primerApellido: this.seleccionForm.value.primer_apellido_apoderado,
      primerNombre: this.seleccionForm.value.primer_nombre_apoderado,
      segundoApellido: this.seleccionForm.value.segundo_apellido_apoderado,
      segundoNombre: this.seleccionForm.value.segundo_nombre_apoderado
    };
    this.representanteLegal = {
      id: '0',
      direcciones: null,
      emails: this.mailRepresentante,
      natural: this.natural ,
      numeroDocumento: this.seleccionForm.value.numero_documento_apoderado,
      tipoDocumento: this.seleccionForm.value.tipo_documento_apoderado.value,
      tipoPersona: 'NA',

    }
    this.denunciado = {
      id: '0',
      empresa: this.denunciadoEmpresa,
      numeroDocumento: this.seleccionForm.value.numero_documento,
      tipoDocumento: this.seleccionForm.value.tipo_documento.value,
      tipoPersona: 'EM',
      emails: this.mail,
      direcciones: this.direcciones,
      representanteLegal: this.representanteLegal,

    };
    sessionStorage.setItem('Denunciado', JSON.stringify(this.denunciado));
    this.router.navigate(['/sic/conducta_alerta']);

  }

  registrarPersonaJuridicaExistente() {
    this.denunciadoEmpresa = {
      descripcion: this.responsePersona.persona.empresa.descripcion,
      digitoVerificacion: this.responsePersona.persona.empresa.digitoVerificacion,
      razonsocial:  this.responsePersona.persona.empresa.razonSocial,
    };
    this.mail = [{
      tipo: 'PE',
      descripcion: this.responsePersona.persona.emails[0].descripcion
    }];
    this.mailRepresentante = [{
      tipo: 'PE',
      descripcion: this.seleccionForm.value.correo_apoderado
    }];
    this.telefono = [{
      tipo: 'CE',
      numero: this.responsePersona.persona.direcciones[0].telefonos[0].numero
    },
      {
        tipo: 'FI',
        numero: this.responsePersona.persona.direcciones[0].telefonos[0].numero
      },
      {
        tipo: 'FX',
        numero: this.responsePersona.persona.direcciones[0].telefonos[0].numero
      }
    ];
    this.direcciones = [{
      codigoPais:  this.responsePersona.persona.direcciones[0].codigoPais,
      tipo: 'PE',
      descripcion:  this.responsePersona.persona.direcciones[0].descripcion,
      codigoCiudad:  this.responsePersona.persona.direcciones[0].codigoCiudad,
      codigoRegion:  this.responsePersona.persona.direcciones[0].codigoRegion,
      telefonos: this.telefono
    }];
    this.natural = {
      primerApellido: this.seleccionForm.value.primer_apellido_apoderado,
      primerNombre: this.seleccionForm.value.primer_nombre_apoderado,
      segundoApellido: this.seleccionForm.value.segundo_apellido_apoderado,
      segundoNombre: this.seleccionForm.value.segundo_nombre_apoderado
    };
    this.representanteLegal = {
      id: '0',
      direcciones: null,
      emails: this.mailRepresentante,
      natural: this.natural ,
      numeroDocumento: this.seleccionForm.value.numero_documento_apoderado,
      tipoDocumento: this.seleccionForm.value.tipo_documento_apoderado.value,
      tipoPersona: 'NA',

    }
    this.denunciado = {
      id: '0',
      empresa: this.denunciadoEmpresa,
      numeroDocumento: this.responsePersona.persona.numeroDocumento,
      tipoDocumento: this.responsePersona.persona.tipoDocumento,
      tipoPersona: 'EM',
      emails: this.mail,
      direcciones: this.direcciones,
      representanteLegal: this.representanteLegal,

    };
    sessionStorage.setItem('Denunciado', JSON.stringify(this.denunciado));
    this.router.navigate(['/sic/conducta_alerta']);

  }
  GetPersona() {
    this.requestUsuarioxDocumento = {
      tipoDocumento: this.seleccionForm.value.tipo_documento.value,
      numeroDocumento: this.seleccionForm.value.numero_documento,

    };
    this.sicUtils.postConsultarPersonaXDocumento(this.requestUsuarioxDocumento).subscribe(
      // Success response
      response => {
        this.responsePersona = response;
        // alert(this.responsePersona.persona.natural.primerApellido);
        this.precargarDatos();
        this.proteccionDatos = true;
      },
      // Failure response
      error => {
        this.DatosOcultar = false;
        console.error(error);
      },
    );
  }
  GetPersonaRL() {
    this.requestUsuarioxDocumento = {
      tipoDocumento: this.seleccionForm.value.tipo_documento_apoderado.value,
      numeroDocumento: this.seleccionForm.value.numero_documento_apoderado,

    };
    this.sicUtils.postConsultarPersonaXDocumento(this.requestUsuarioxDocumento).subscribe(
      // Success response
      response => {
        this.responsePersona2 = response;
        // alert(this.responsePersona.persona.natural.primerApellido);
        this.precargarDatosRL();
      },
      // Failure response
      error => {
        this.DatosOcultar = false;
        console.error(error);
      },
    );
  }
  precargarDatos() {
    this.seleccionForm.controls['razon_social'].setValue(this.responsePersona.persona.empresa.razonSocial);
    this.seleccionForm.controls['celular'].setValue('3000000');
    this.seleccionForm.controls['pais'].setValue( 'CO');
    this.seleccionForm.controls['ciudad'].setValue( '6');
    this.seleccionForm.controls['departamento'].setValue( '0');
    this.seleccionForm.controls['direccion'].setValue('calle 1 - 0 - 0');
    this.seleccionForm.controls['correo'].setValue('correo@correo.gov.co');
    this.seleccionForm.controls['razon_social'].disable();
    this.seleccionForm.controls['tipo_documento'].disable();
    this.seleccionForm.controls['numero_documento'].disable();
  }
  precargarDatosRL() {
    this.seleccionForm.controls['primer_nombre_apoderado'].setValue(this.responsePersona2.persona.natural.primerNombre);
    this.seleccionForm.controls['segundo_nombre_apoderado'].setValue(this.responsePersona2.persona.natural.segundoNombre);
    this.seleccionForm.controls['primer_apellido_apoderado'].setValue(this.responsePersona2.persona.natural.primerApellido);
    this.seleccionForm.controls['segundo_apellido_apoderado'].setValue(this.responsePersona2.persona.natural.segundoApellido);
    this.seleccionForm.controls['correo_apoderado'].setValue(this.responsePersona2.persona.emails[0].descripcion);
    this.seleccionForm.controls['primer_nombre_apoderado'].disable();
    this.seleccionForm.controls['segundo_nombre_apoderado'].disable();
    this.seleccionForm.controls['primer_apellido_apoderado'].disable();
    this.seleccionForm.controls['segundo_apellido_apoderado'].disable();
    this.seleccionForm.controls['correo_apoderado'].disable();

  }

}
