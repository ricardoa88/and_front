import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatosPersonaJuridicaForm } from './datos-persona-juridica-form';
import { Router } from '@angular/router';
import { SicUtilsService } from '../../services/sic-utils.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalService } from '@shared/dialog-modal/services/confirm-modal.service';
import jsonStrings from '@stringResources/tramites/denuncia-infraccion.json';
import { Persona, Natural, Empresa, DireccionesEntity, EmailsEntity, TelefonosEntity, responseService } from '../../models/sic-models';
@Component({
  selector: 'app-datos-persona-juridica',
  templateUrl: './datos-persona-juridica.component.html',
  styleUrls: ['./datos-persona-juridica.component.scss']
})
export class DatosPersonaJuridicaComponent implements OnInit {


  mostrarDireccion: boolean;
  seleccionForm: FormGroup;
  seleccionSolucionForm: DatosPersonaJuridicaForm;
  invalidForm = false;
  listaCalleCarrera: any = [];
  listaInformacionComplementaria: any = [];
  listaTipoDocumento: any = [];
  listaPais: any = [];
  listaDepartamento: any = [];
  listaCiudad: any = [];
  listaGenero: any = [];
  listaEmpresa: any = [];
  listaEmpresaPrivada: any = [];
  currentDate: NgbDateStruct;
  maxDate: NgbDateStruct;
  textoTerminosCondiciones: string;
  direccionCorrespondencia = 'Ej: Avenida Calle 32 # 13-83';
  informacionComplementaria = 'Ej: Edificio Bávaro, apto 504 torre 2';
  tempDireccion: string;
  tempComplementos: string;
  persona: Persona;
  empresa: Empresa;
  natural: Natural;
  direcciones: DireccionesEntity[];
  mail: EmailsEntity[];
  telefono: TelefonosEntity[];
  direccionCambiada = false;
  contadorComplementos = 0;
  listaCalleLetra: any = [
    { text: 'A', value: 'A' },
    { text: 'B', value: 'B' },
    { text: 'C', value: 'C' },
    { text: 'D', value: 'D' },
    { text: 'E', value: 'E' },
    { text: 'F', value: 'F' },
    { text: 'G', value: 'G' },
    { text: 'H', value: 'H' },
    { text: 'I', value: 'I' }];
  listaBis: any = [
    { text: 'Bis', value: 'Bis' }
  ];
  listaSufijoCardinal: any = [
    { text: 'Norte', value: 'Norte' },
    { text: 'Sur', value: 'Sur' },
    { text: 'Oriente', value: 'Oriente' },
    { text: 'Occidente', value: 'Occidente' }
  ];
  codigov: string;
  i: number;
  digitoVerificacion: any;
  CodigoVerificacion: any;
  listaDigitoverificacion: any = [
    { text: '0', value: '0' },
    { text: '1', value: '1' },
    { text: '2', value: '2' },
    { text: '3', value: '3' },
    { text: '4', value: '4' },
    { text: '5', value: '5' },
    { text: '6', value: '6' },
    { text: '7', value: '7' },
    { text: '8', value: '8' },
    { text: '9', value: '9' }
  ];

  constructor(private router: Router, private sicUtils: SicUtilsService, private modalAlertService: ConfirmModalService) { }
  ngOnInit() {
    this.seleccionSolucionForm = new DatosPersonaJuridicaForm();
    this.mostrarDireccion = true;
    this.buildForm();
    this.cargarListasGenericas();
    this.precargarDatosFormAnterior();
    this.setCurrentDate();
    this.cargarTextoTerminosCondiciones();
    this.setMaxDate();
  }
  precargarDatosFormAnterior() {
    this.seleccionForm.controls.tipo_documento.setValue(sessionStorage.getItem('textotipoDocumento'));
    this.seleccionForm.controls.numero_documento.setValue(sessionStorage.getItem('numeroDocumento'));
    this.seleccionForm.controls.correo.setValue(sessionStorage.getItem('correo'));
  }
  setCurrentDate(): void {
    const currentDate = new Date();
    this.currentDate = {
      day: currentDate.getDate(),
      month: (currentDate.getMonth() + 1),
      year: currentDate.getFullYear()
    };
  }

  setMaxDate(): void {
    const maxDate = new Date();
    this.maxDate = {
      day: maxDate.getDate(),
      month: (maxDate.getMonth() + 1),
      year: (maxDate.getFullYear() - 15)
    };
  }

  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }

  OcultarDireccion() {
    this.mostrarDireccion = !this.mostrarDireccion;
    this.limpiarDireccion();
  }

  agregarDireccionDefinitiva() {
    let segundo_valor = '';
    let tercer_valor = '';
    if (this.seleccionForm.value.segundo_valor_calle_cra.length > 0) {
      segundo_valor = ' # ' + this.seleccionForm.value.segundo_valor_calle_cra;
    }
    if (this.seleccionForm.value.tercer_valor_calle_cra.length > 0) {
      tercer_valor = ' - ' + this.seleccionForm.value.tercer_valor_calle_cra;
    }
    this.tempDireccion = this.seleccionForm.value.calle_cra.value + '' +
      this.seleccionForm.value.primer_valor_calle_cra + ' ' +
      this.seleccionForm.value.calle_letra.value + ' ' +
      this.seleccionForm.value.calle_bis.value + ' ' +
      this.seleccionForm.value.calle_bis_letra.value + ' ' +
      this.seleccionForm.value.calle_sufijo.value + ' ' +
      segundo_valor + ' ' +
      this.seleccionForm.value.calle_letra_segundo.value + ' ' +
      tercer_valor + ' ' +
      this.seleccionForm.value.calle_sufijo_tercer.value;
    this.tempDireccion = this.tempDireccion.split('undefined').join('').replace(/\s\s+/g, ' ');
    this.seleccionForm.controls.direccion_definitiva.setValue(this.tempDireccion);
    this.direccionCambiada = true;
  }

  agregarComplementos() {
    this.contadorComplementos++;
    if (this.contadorComplementos < 3) {
      this.tempComplementos += ' ' + this.seleccionForm.value.informacion_complementaria.value + ' ' +
        this.seleccionForm.value.informacion_complementaria2;
      this.seleccionForm.controls.direccion_definitiva.setValue(this.tempDireccion + ' ' +
        this.tempComplementos.split('undefined').join(''));
      this.seleccionForm.controls.informacion_complementaria.setValue('');
      this.seleccionForm.controls.informacion_complementaria2.setValue('');
    }
  }

  limpiarControl(nombre: string) {
    this.seleccionForm.controls[nombre].setValue('');
  }
  limpiarDireccion() {
    this.tempDireccion = '';
    this.tempComplementos = '';
    const controles = ['calle_cra', 'primer_valor_calle_cra', 'calle_letra', 'calle_bis', 'calle_bis_letra', 'calle_sufijo',
      'segundo_valor_calle_cra', 'calle_letra_segundo', 'tercer_valor_calle_cra', 'calle_sufijo_tercer', 'informacion_complementaria',
      'informacion_complementaria2', 'direccion_definitiva'];
    for (const control of controles) {
      this.limpiarControl(control);
    }
    this.contadorComplementos = 0;
  }

  guardarDireccionDefinitiva() {
    if (this.tempDireccion.length > 0) {
      this.direccionCambiada = true;
      this.direccionCorrespondencia = this.tempDireccion.split('undefined').join('');
      this.informacionComplementaria = this.tempComplementos.split('undefined').join('');
      this.contadorComplementos = 0;
      this.OcultarDireccion();
    }
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.seleccionForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        console.error(name);
      }
    }
    return invalid;
  }

  siguiente() {
    this.findInvalidControls();
    if (this.seleccionSolucionForm.isValid()) {
      this.registrarPersona();
    } else {
      this.invalidForm = true;
      return;
    }
  }

  lanzarModal() {
    this.modalAlertService.openDialogCustom(
      jsonStrings.messages.modal_aviso_privacidad,
      this.textoTerminosCondiciones,
      [{
        name: 'Aceptar',
        value: true,
        styleClass: 'btn-high',
        event: () => {
          this.router.navigate(['/sic/usuario_clave_acceso']);
        }
      }
      ],
      'none',
    );
  }

  registrarPersona() {
    this.natural = {
      saludo: 'ES',
      primerNombre: '',
      segundoNombre:  '',
      primerApellido:  '',
      segundoApellido:  '',
      escolaridad: null,
      genero:  null,
      grupoEtnico: null,
    };
    this.mail = [{
      tipo: 'PE',
      descripcion: this.seleccionForm.value.correo
    }];
    this.telefono = [{
      tipo: 'CE',
      numero: this.seleccionForm.value.celular
    }
    ];
    this.direcciones = [{
      codigoPais: this.seleccionForm.value.pais.value,
      tipo: 'EL',
      descripcion: this.direccionCorrespondencia + ' ' + this.informacionComplementaria,
      codigoCiudad: this.seleccionForm.value.ciudad.value,
      codigoRegion: this.seleccionForm.value.departamento.value,
      telefonos: this.telefono
    }];
    this.empresa = {
      descripcion: this.seleccionForm.value.descripcion,
      digitoVerificacion: this.seleccionForm.value.digitoVerificacion.value,
      otroTipoEmpresaPrivada: this.seleccionForm.value.otro_tipo_empresa,
      razonSocial: this.seleccionForm.value.razon_social,
      tipoEmpresa: this.seleccionForm.value.tipo_empresa.value,
      tipoEmpresaPrivada: this.seleccionForm.value.tipo_empresa_privada.value
    };
    console.log(this.empresa)
    this.persona = {
      tipoPersona: sessionStorage.getItem('tipoPersona'),
      numeroDocumento: this.seleccionForm.value.numero_documento,
      tipoDocumento: sessionStorage.getItem('tipoDocumento'),
      natural: this.natural,
      empresa: this.empresa,
      emails: this.mail,
      direcciones: this.direcciones
    };

    console.error(this.persona);
    sessionStorage.setItem('empresa', JSON.stringify(this.persona));
    this.sicUtils.postRegistrarPersona(this.persona)
      .subscribe((data: responseService) => {
          if (data.persona != null) {
            console.log( data.persona.id.toString())
            sessionStorage.setItem('idPersona', data.persona.id.toString())
            this.lanzarModal();
          } else {
            console.error(data);
            return;
          }
        }, (error) => {
          console.error(error);
        }
      );
  }

  cargarTextoTerminosCondiciones() {
    this.sicUtils.getListaGenericas('TERMINOS_CONDICIONES')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.textoTerminosCondiciones = data[0].text;
          }
        }, (error) => {
          console.error(error);
        }
      );
  }

  limpiar() {
    this.invalidForm = false;
    this.seleccionForm.reset();
  }

  cargarListaDepartamento() {
    this.seleccionForm.controls.departamento.reset();
    this.sicUtils.getListaRegion(this.seleccionForm.value.pais.value)
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaDepartamento = data;
          }
        }, (error) => {
          console.error(error);
        }
      );
  }

  cargarListaCiudad() {
    this.seleccionForm.controls.ciudad.reset();
    this.sicUtils.getListaCiudad(this.seleccionForm.value.departamento.value)
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaCiudad = data;
          }
        }, (error) => {
          console.error(error);
        }
      );
  }
caculardv() {
this.codigov = this.seleccionForm.value.numero_documento;
const primos = [3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 67, 71];
const max = this.codigov.length;
console.log(max);
const nitInvertido = [];
let x = 0;
let sumatoria = 0;
for (this.i = max - 1 ; this.i >= 0; this.i--) {
    nitInvertido[x] = this.codigov.charAt(this.i);
    sumatoria = sumatoria + (nitInvertido[x] * primos[x]);
    x++;
  }
const residuo = sumatoria % 11;
console.log(residuo);
if (residuo === 0 || residuo === 1) {
  this.CodigoVerificacion = residuo;
  } else {
  this.CodigoVerificacion  = 11 - residuo;
  }
console.error(  this.CodigoVerificacion, this.seleccionForm.value.digitoVerificacion.value);
if ( this.CodigoVerificacion == this.seleccionForm.value.digitoVerificacion.value) {
  } else {
  this.lanzarModalcodigo();
  }
}
  lanzarModalcodigo() {
    this.modalAlertService.openDialogCustom(
      'Alerta',
      'Revisar el codigo de verificación',
      [{
        name: 'ACEPTAR',
        value: true,
        styleClass: 'btn-middle',
        event: () => {
        }
      }
      ],
      'warning',
    );
  }
  cargarListasGenericas() {
    this.sicUtils.getListaGenericas('TIPO_VIA_PRINCIPAL')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaCalleCarrera = data;
          }
        }, (error) => {
          console.error(error);
        }
      );
    this.sicUtils.getListaGenericas('COMPLEMENTOS_DIRECCION')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaInformacionComplementaria = data;
          }
        }, (error) => {
          console.error(error);
        }
      );

    // Tipo de documento
    this.sicUtils.getListaGenericas('TIPO_DOCUMENTO_PERSONA')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaTipoDocumento = data;
          }
        }, (error) => {
          console.error(error);
        }
      );

    // Pais
    this.sicUtils.getListaGenericas('PAIS')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaPais = data;
          }
        }, (error) => {
          console.error(error);
        }
      );

    // Genero
    this.sicUtils.getListaGenericas('GENERO')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaGenero = data;
          }
        }, (error) => {
          console.error(error);
        }
      );

    // Tipo de empresa
    this.sicUtils.getListaGenericas('TIPO_EMPRESA')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaEmpresa = data;
          }
        }, (error) => {
          console.error(error);
        }
      );

    // Tipo de empresa privada
    this.sicUtils.getListaGenericas('TIPO_EMPRESA_PRIVADA')
      .subscribe((data: any[]) => {
          if (data.length > 0) {
            this.listaEmpresaPrivada = data;
          }
        }, (error) => {
          console.error(error);
        }
      );
  }
}
