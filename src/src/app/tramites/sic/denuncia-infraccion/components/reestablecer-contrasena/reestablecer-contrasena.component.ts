import { Component, OnInit } from '@angular/core';
import { ConfirmModalService } from '@shared/dialog-modal/services/confirm-modal.service';
import { FormGroup } from '@angular/forms';
import { ReestablecerContrasenaForm } from './reestablecer-contrasena-form';
import jsonStrings from '@stringResources/tramites/denuncia-infraccion.json';
import { SicUtilsService } from '../../services/sic-utils.service';
import { CambiarPass, responseService} from '../../models/sic-models';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-reestablecer-contrasena',
  templateUrl: './reestablecer-contrasena.component.html',
  styleUrls: ['./reestablecer-contrasena.component.scss']
})
export class ReestablecerContrasenaComponent implements OnInit {
  tok: { t: string};
  cambiarPass: CambiarPass;
  seleccionForm: FormGroup;
  seleccionSolucionForm: ReestablecerContrasenaForm;
  busquedavacia: string;
  invalidForm = false;
  private results: responseService;

  constructor(private sicUtilsService: SicUtilsService,
              private router: Router,
              private modalAlertService: ConfirmModalService,
              private RutaActiva: ActivatedRoute) { }

   ngOnInit() {
    this.seleccionSolucionForm = new ReestablecerContrasenaForm();
    this.buildForm();
    this.busquedavacia = jsonStrings.messages.error_reestablecer_password;
  }

  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }
  enviar() {
    if (!this.seleccionForm.valid) {
      this.invalidForm = true;
      this.cambiarPass = {
        newPassword: this.seleccionForm.value.nuevo_password,
        sistema: 'SL',
        token: this.RutaActiva.snapshot.params.t,
      };
      console.log(this.cambiarPass);
    }
    this.invalidForm = false;
    console.log(this.cambiarPass);
  }
  enviarPass() {
    if (this.seleccionSolucionForm.isValid()) {
      this.tok = {
        t: this.RutaActiva.snapshot.params.t,
      };
      this.cambiarPass = {
        newPassword: this.seleccionForm.value.nuevo_password,
        sistema: 'RC',
        token: this.tok.t,
      };
      console.log( this.cambiarPass );

      this.sicUtilsService.consPostCambiarPass(this.cambiarPass).subscribe(
        response => {
          console.error(response);
          if (response.mensaje === null) {
            this.results = response;
            this.OpenModalSuccess();
            console.error(this.results.mensaje);
          } else {
            console.error('No responde el servicio');
          }
        },
        error => {
          console.error(error);
        },
      );
    } else {
      this.invalidForm = true;
      return;
    }
  }
  OpenModalSuccess() {
    this.modalAlertService.openDialogCustom(
      'Proceso Exitoso',
      'La clave fue cambiada con éxito',
      [{
        name: 'ACEPTAR',
        value: true,
        styleClass: 'btn-middle',
        event: () => {
          this.router.navigate(['/sic/iniciar_sesion']);
        }
      }
      ], 'success');
  }
  accion_continuar() {
    this.router.navigate(['/sic/iniciar_sesion']);
  }
}
