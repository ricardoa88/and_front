import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmedValidator } from './ConfirmedValidator';

export class ReestablecerContrasenaForm {
    public form: FormGroup;
    private formBuilder: any;

    constructor() {
        this.formBuilder = new FormBuilder();
        this.buildForm();
    }

    buildForm() {
        this.form = this.formBuilder.group({
            nuevo_password: ['', Validators.required],
            repetir_password: ['', Validators.required]
        }, {
          validator: ConfirmedValidator('nuevo_password', 'repetir_password')
        });
    }

    getForm(): FormGroup {
        return this.form;
    }

    getFormValues() {
        return this.form.value;
    }

    isValid() {
        return this.form.valid;
    }

}
