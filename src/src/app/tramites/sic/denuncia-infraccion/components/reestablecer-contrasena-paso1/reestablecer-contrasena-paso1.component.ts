import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ReestablecerContrasenaPaso1Form } from './reestablecer-contrasena-paso1-form';
import { SicUtilsService } from '../../services/sic-utils.service';
import jsonStrings from '@stringResources/tramites/denuncia-infraccion.json';
import {Recuperarpass, responseService} from '../../models/sic-models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reestablecer-contrasena-paso1',
  templateUrl: './reestablecer-contrasena-paso1.component.html',
  styleUrls: ['./reestablecer-contrasena-paso1.component.scss']
})
export class ReestablecerContrasenaPaso1Component implements OnInit {
  recuperarPass: Recuperarpass;
  seleccionForm: FormGroup;
  seleccionSolucionForm: ReestablecerContrasenaPaso1Form;
  invalidForm = false;
  listaTipoDocumento: any = [];
  mensajeEnviado = false;
  messages: any;
  private results: responseService;

  constructor(private sicUtilsService: SicUtilsService, private router: Router ) { }

  ngOnInit() {
    this.seleccionSolucionForm = new ReestablecerContrasenaPaso1Form();
    this.buildForm();
    this.cargarListasGenericas();
  }

  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }

  setValidator() {
    if (this.seleccionForm.value.tipo_documento.value === 'PA') {
      this.seleccionForm.get('numero_documento').setValidators([Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12),
        Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]);
      this.seleccionForm.get('numero_documento').updateValueAndValidity();
    } else {
      this.seleccionForm.controls.numero_documento.setValidators([Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12),
        Validators.pattern('^[0-9]+$')]);
      this.seleccionForm.controls.numero_documento.updateValueAndValidity();
    }
  }
  consultar() {
    if (this.seleccionSolucionForm.isValid()) {
      this.messages = {
        warning_envio_exitoso: jsonStrings.messages.warning_envio_exitoso.split('%').join(this.seleccionForm.value.correo)
      };
      this.mensajeEnviado = true;
    } else {
      this.invalidForm = true;
      return;
    }
  }
  enviar() {
    if (this.seleccionSolucionForm.isValid()) {
      this.recuperarPass = {
        email: this.seleccionForm.value.correo,
        enviarCorreo: true,
        numeroDocumento: this.seleccionForm.value.numero_documento,
        redireccionURL: 'http://localhost:4200/sic/reestablecer_contrasena_asignar',
        sistema: 'RC',
        tipoDocumento: this.seleccionForm.value.tipo_documento.value,
      };
      console.log( this.recuperarPass );

      this.sicUtilsService.consPostRecuperarPass(this.recuperarPass).subscribe(
        response => {
          console.error(response);
          if (response.mensaje === null) {
            this.results = response;
            this.accion_continuar();
          } else {
            this.messages = {
              warning_envio_exitoso: (response.mensaje)
            };
            this.mensajeEnviado = true;
            console.error('No responde el servicio');
          }
        },
        error => {
          console.error(error);
        },
      );
    } else {
      this.invalidForm = true;
      return;
    }
  }
  accion_continuar() {
    this.messages = {
      warning_envio_exitoso: jsonStrings.messages.warning_envio_exitoso.split('%').join(this.seleccionForm.value.correo)
    };
    this.mensajeEnviado = true;
    console.error(this.results.mensaje);
  }
  cargarListasGenericas() {
    // Tipo de documento
    this.sicUtilsService.getListaGenericas('TIPO_DOCUMENTO_PERSONA')
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaTipoDocumento = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }
}
