import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AdjuntaDocumentoForm} from './adjunta-documento-form'
import { Router} from '@angular/router'
import jsonStrings from '@stringResources/tramites/denuncia-infraccion.json';
import {Adjuntos} from '../../models/sic-models';
import {FileUploader} from 'ng2-file-upload';
import { ConfirmModalService } from '@shared/dialog-modal/services/confirm-modal.service';
@Component({
  selector: 'app-adjunta-documento',
  templateUrl: './adjunta-documento.component.html',
  styleUrls: ['./adjunta-documento.component.scss']
})
export class AdjuntaDocumentoComponent implements OnInit {
  submitted = false;
  isError = true;
  seleccionForm: FormGroup;
  seleccionSolucionForm: AdjuntaDocumentoForm;
  messages: any;
  invalidForm = false;
  maxfileerror: any;
  public uploader: FileUploader = new FileUploader({
    disableMultipart: false,
    maxFileSize: 3242880,
    queueLimit : 5
  });
  public  vadjunto: any[] = [];
  adjuntos: Adjuntos[] ;
  observacion: string;
  constructor(private router: Router, private modalAlertService: ConfirmModalService) {
    this.uploader.onWhenAddingFileFailed = (fileItem) => {
      this.maxfileerror = true;
    };
  }


  ngOnInit() {
    this.seleccionSolucionForm = new AdjuntaDocumentoForm();
    this.messages = {
      warning_solicitud_soportada : jsonStrings.messages.warning_solicitud_soportada
    };
    this.buildForm();
  }

  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }


  public findInvalidControls() {
    const invalid = [];
    const controls = this.seleccionForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        console.error(name);

      }
    }
    return invalid;
  }

  accion_continuar(){
    this.findInvalidControls();
    if (this.seleccionSolucionForm.isValid()) {
      this.router.navigate(['/sic/enviar_solicitud']);
    }
    else {
      this.invalidForm = true;
      return;
    }
  }

  accion_anterior(){
    this.router.navigate(['/sic/conducta_alerta']);
  }

  public onFileSelected(File): void {
    console.log('Ficheros adjuntados');
    for (let cpt = 0; cpt < File.length; cpt++) {
      const currentFile = File[cpt];
      currentFile.numeroAdjunto = cpt;
      let variable: any;
      variable = {};
      const reader = new FileReader();
      reader.readAsDataURL(currentFile);
      reader.onload = (event) => {
        variable.contenidoArchivo_BASE64 = reader.result;
        variable.numeroAdjunto = this.vadjunto.length;
        variable.nombreArchivo = currentFile.name;
        this.vadjunto.push(variable);
        console.log(this.vadjunto, this.vadjunto.length);
        sessionStorage.setItem('adjuntos', JSON.stringify(this.vadjunto)); // salida para enviar al servicio
      };
    }
    if ( this.vadjunto.length ==  4) {
      this.lanzarModalInfo();
      this.seleccionForm.controls.adjuntos.disable();
    }
  }
  sessionRemove() {
    sessionStorage.removeItem('adjuntos');
    this.vadjunto = [];
    this.seleccionForm.controls.adjuntos.enable();

  }
  lanzarModalInfo() {
    this.modalAlertService.openDialogCustom(
      'Alerta',
      'Puede cargar máximo 5 archivos',
      [{
        name: 'ACEPTAR',
        value: true,
        styleClass: 'btn-middle',
        event: () => {
        }
      }
      ],
      'warning',
    );
  }

  mensajeOut() {
    this.maxfileerror = false;
  }


  ///////


}
