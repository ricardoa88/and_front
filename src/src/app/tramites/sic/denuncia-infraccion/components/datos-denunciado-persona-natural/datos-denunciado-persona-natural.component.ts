/* tslint:disable:no-string-literal */
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { DatosDenunciadoPersonaNaturalForm } from './datos-denunciado-persona-natural-form';
import { Router } from '@angular/router';
import { SicUtilsService } from '../../services/sic-utils.service';
import { DireccionesEntity, EmailsEntity, TelefonosEntity, Denunciado } from '../../models/sic-models';
import { Persona, Natural, responseService, requestUsuarioxDocumento } from '../../models/sic-models';
@Component({
  selector: 'app-datos-denunciado-persona-natural',
  templateUrl: './datos-denunciado-persona-natural.component.html',
  styleUrls: ['./datos-denunciado-persona-natural.component.scss']
})
export class DatosDenunciadoPersonaNaturalComponent implements OnInit {
  responsePersona: responseService;
  requestUsuarioxDocumento: requestUsuarioxDocumento;
  persona: Persona;
  DatosOcultar: boolean;
  proteccionDatos = false;
  seleccionForm: FormGroup;
  seleccionSolucionForm: DatosDenunciadoPersonaNaturalForm;
  invalidForm = false;
  listaTipoDocumento: any = [];
  listaPais: any = [];
  listaDepartamento: any = [];
  listaCiudad: any = [];
  natural: Natural;
  denunciado: Denunciado;
  direcciones: DireccionesEntity[];
  mail: EmailsEntity[];
  telefono: TelefonosEntity[];
  private numeronull: string;

  constructor(private router: Router, private sicUtils: SicUtilsService) { }

  ngOnInit() {
    this.seleccionSolucionForm = new DatosDenunciadoPersonaNaturalForm();
    this.buildForm();
    this.cargarListasGenericas();
  }


  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.seleccionForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        console.error(name);
      }
    }
    return invalid;
  }

  limpiar() {
    this.invalidForm = false;
    this.seleccionForm.reset();
    this.seleccionForm.enable();
    this.proteccionDatos = false;
  }
  accion_continuar() {
    this.findInvalidControls();
    if (this.seleccionSolucionForm.isValid()) {
      this.registrarPersona();
    } else {
      this.invalidForm = true;
      return;
    }
  }
  siguiente() {
    this.findInvalidControls();
    if (this.seleccionSolucionForm.isValid()) {
      if (this.responsePersona.persona == null) {
        this.proteccionDatos = true;
        this.registrarPersona();
      } else {
        this.registrarPersonaExistente();
      }
    } else {
      this.invalidForm = true;
      return;
    }
  }
  setValidator() {
    if (this.seleccionForm.value.tipo_documento.value === 'PA') {
      this.seleccionForm.get('numero_documento').setValidators([Validators.required,
        Validators.minLength(4), Validators.maxLength(12),
        Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]);
      this.seleccionForm.get('numero_documento').updateValueAndValidity();
    } else {
      this.seleccionForm.controls['numero_documento'].setValidators([Validators.required,
        Validators.minLength(4), Validators.maxLength(12),
        Validators.pattern('^[0-9]+$')]);
      this.seleccionForm.controls['numero_documento'].updateValueAndValidity();
    }
  }

  accion_anterior() {
    this.router.navigate(['/sic/datos_denunciante']);
  }

  cargarListaDepartamento() {
    this.seleccionForm.controls['departamento'].reset();
    this.sicUtils.getListaRegion(this.seleccionForm.value.pais.value)
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaDepartamento = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  cargarListaCiudad() {
    this.seleccionForm.controls['ciudad'].reset();
    this.sicUtils.getListaCiudad(this.seleccionForm.value.departamento.value)
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaCiudad = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  cargarListasGenericas() {
    // Tipo de documento
    this.sicUtils.getListaGenericas('TIPO_DOCUMENTO_PERSONA')
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaTipoDocumento = data;
        }
      }, (error) => {
        console.error(error);
      }
      );

    // Pais
    this.sicUtils.getListaGenericas('PAIS')
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaPais = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  registrarPersona() {
    this.mail = [{
      tipo: 'PE',
      descripcion: this.seleccionForm.value.correo
    }];
    this.telefono = [{
      tipo: 'CE',
      numero: this.seleccionForm.value.celular
    },
    {
      tipo: 'FI',
      numero: this.seleccionForm.value.telefono_fijo
    },
    {
    tipo: 'FX',
    numero: this.seleccionForm.value.fax
    }
    ];
    this.direcciones = [{
      codigoPais: this.seleccionForm.value.pais.value,
      tipo: 'PE',
      descripcion: this.seleccionForm.value.direccion,
      codigoCiudad: this.seleccionForm.value.ciudad.value,
      codigoRegion: this.seleccionForm.value.departamento.value,
      telefonos: this.telefono
    }];
    this.natural = {
      saludo: 'ES',
      primerApellido: this.seleccionForm.value.primer_apellido,
      primerNombre: this.seleccionForm.value.primer_nombre,
      segundoApellido: this.seleccionForm.value.segundo_apellido,
      segundoNombre: this.seleccionForm.value.segundo_nombre
    };

    this.persona = {
      tipoPersona: 'NA',
      numeroDocumento: this.seleccionForm.value.numero_documento,
      tipoDocumento: this.seleccionForm.value.tipo_documento.value,
      natural: this.natural,
      emails: this.mail,
      direcciones: this.direcciones
    };
    sessionStorage.setItem('Denunciado', JSON.stringify(this.denunciado));
    this.router.navigate(['/sic/conducta_alerta']);
  }
  registrarPersonaExistente() {
    this.natural = {
      saludo: 'ES',
      primerNombre: this.responsePersona.persona.natural.primerNombre,
      segundoNombre: this.responsePersona.persona.natural.segundoNombre,
      primerApellido: this.responsePersona.persona.natural.primerApellido,
      segundoApellido: this.responsePersona.persona.natural.segundoApellido,
    };
    this.mail = [{
      tipo: 'PE',
      descripcion: this.responsePersona.persona.emails[0].descripcion
    }];
    this.telefono = [{
      tipo: 'CE',
      numero: this.numeronull
    }
    ];
    this.direcciones = [{
      codigoPais: this.responsePersona.persona.direcciones[0].codigoPais,
      tipo: 'EL',
      descripcion: this.responsePersona.persona.direcciones[0].descripcion,
      codigoCiudad: this.responsePersona.persona.direcciones[0].codigoCiudad,
      codigoRegion: this.responsePersona.persona.direcciones[0].codigoRegion,
      telefonos: this.telefono
    }];
    this.persona = {
      tipoPersona: 'NA',
      numeroDocumento: this.responsePersona.persona.numeroDocumento,
      tipoDocumento: this.responsePersona.persona.tipoDocumento,
      natural: this.natural,
      emails: this.mail,
      direcciones: this.direcciones
    };
    console.error(this.persona);
    sessionStorage.setItem('Denunciado', JSON.stringify(this.persona));
    this.router.navigate(['/sic/conducta_alerta']);
  }
  GetPersona() {
    this.requestUsuarioxDocumento = {
      tipoDocumento: this.seleccionForm.value.tipo_documento.value,
      numeroDocumento: this.seleccionForm.value.numero_documento,

    };
    this.sicUtils.postConsultarPersonaXDocumento(this.requestUsuarioxDocumento).subscribe(
      // Success response
      response => {
        this.responsePersona = response;
        // alert(this.responsePersona.persona.natural.primerApellido);
        this.precargarDatos();
        this.proteccionDatos = true;
      },
      // Failure response
      error => {
        this.DatosOcultar = false;
        console.error(error);
      },
    );
  }
  precargarDatos() {
    this.seleccionForm.controls['primer_nombre'].setValue(this.responsePersona.persona.natural.primerNombre);
    this.seleccionForm.controls['segundo_nombre'].setValue(this.responsePersona.persona.natural.segundoNombre);
    this.seleccionForm.controls['primer_apellido'].setValue(this.responsePersona.persona.natural.primerApellido);
    this.seleccionForm.controls['segundo_apellido'].setValue(this.responsePersona.persona.natural.segundoApellido);
    this.seleccionForm.controls['celular'].setValue('3000000');
    this.seleccionForm.controls['pais'].setValue( 'CO');
    this.seleccionForm.controls['ciudad'].setValue( '6');
    this.seleccionForm.controls['departamento'].setValue( '1');
    this.seleccionForm.controls['direccion'].setValue( 'Cll 0 1 1');
    this.seleccionForm.controls['correo'].setValue('correo@protecciondedatos.com');
    this.seleccionForm.controls['primer_nombre'].disable();
    this.seleccionForm.controls['segundo_nombre'].disable();
    this.seleccionForm.controls['primer_apellido'].disable();
    this.seleccionForm.controls['segundo_apellido'].disable();
    this.seleccionForm.controls['tipo_documento'].disable();
    this.seleccionForm.controls['numero_documento'].disable();
  }
}
