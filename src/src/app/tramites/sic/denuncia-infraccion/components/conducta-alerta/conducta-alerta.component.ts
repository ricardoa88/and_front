import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl} from '@angular/forms';
import { ConductaAlertaForm } from './conducta-alerta-form';
import { Router } from '@angular/router';
import { SicUtilsService } from '../../services/sic-utils.service';
import { ConfirmModalService } from '@shared/dialog-modal/services/confirm-modal.service';

@Component({
  selector: 'app-conducta-alerta',
  templateUrl: './conducta-alerta.component.html',
  styleUrls: ['./conducta-alerta.component.scss']
})
export class ConductaAlertaComponent implements OnInit {
  submitted = false;
  seleccionForm: FormGroup;
  seleccionSolucionForm: ConductaAlertaForm;
  listaOpcionAlerta: any[];
  invalidForm: boolean;
  codigosTipoAlterta: number[];
  hechos: string;
  isError = true;
  descripcion: string;
  private checkArray: any[];
  constructor(private modalAlertService: ConfirmModalService, private router: Router, private sicUtils: SicUtilsService) { }

  ngOnInit() {
    this.seleccionSolucionForm = new ConductaAlertaForm();
    this.buildForm();
    this.cargar_opciones();
  }
  buildForm() {
    this.seleccionForm = this.seleccionSolucionForm.getForm();
  }
  get f() { return this.seleccionForm.controls; }
  continuar() {
    this.submitted = true;
    this.isError = false;
    if (this.seleccionForm.invalid) {
      return;
    }
    this.accion_continuar();
  }

  lanzarModal() {
    this.modalAlertService.openDialogCustom(
      'No puede seleccionar mas de 4 conductas',
      'Seleccione máximo 4',
      [{
        name: 'ACEPTAR',
        value: true,
        styleClass: 'btn-middle',
        event: () => {
        }
      }
      ],
      'warning',
    );
  }

  accion_anterior() {
    this.router.navigate(['/sic/datos_denuncio']);
  }

  cargar_opciones() {
    this.sicUtils.getListaGenericas('TIPO_CONDUCTA_ALERTA')
      .subscribe((data: any[]) => {
        if (data.length > 0) {
          this.listaOpcionAlerta = data;
        }
      }, (error) => {
        console.error(error);
      }
      );
  }

  changeDescription(opcion){
    this.descripcion = opcion.description;
  }

  onCheckboxChange(e) {
    const checkArray: FormArray = this.seleccionForm.get('checkArray') as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }

    if (checkArray.length > 3) {
      this.lanzarModal();
      return false;
    }
  }

  accion_continuar() {
    if (this.seleccionForm.value.checkArray.length > 4) {
      this.lanzarModal();
      return false;
    }
    if (this.seleccionSolucionForm.isValid()) {
      this.router.navigate(['/sic/adjunta_documento']);
    } else {
      this.invalidForm = true;
      return;
    }
    this.codigosTipoAlterta = [this.seleccionForm.value.opcionTipo];
    this.hechos = this.seleccionForm.value.denuncia;
    sessionStorage.setItem('codigosTipoAlerta', JSON.stringify(this.seleccionForm.value.checkArray));
    sessionStorage.setItem('hechos', JSON.stringify(this.hechos));
  }
  limpiar() {
    this.seleccionForm.reset();
    this.seleccionForm.controls['opcionTipo'].enable();
  }
}
