
export interface SolicitudCertificadoModel {
        primerNombre: string;

        segundoNombre: string;

        primerApellido: string;

        segundoApellido: string; 

        tipoDocumento: string;

        numIdentificacion: string;

        correoElectronico: string ;

        tipoEntidad: string;

        pais: string ;
}