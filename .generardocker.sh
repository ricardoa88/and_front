#!/bin/bash
# FUNCIONA DIOS SANTO

ARGM=$1

case $ARGM in
  DUPS)
    rm -f .env;
    cp -fpvT .docker/.env.dev.dist .env;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . up --build --force-recreate --remove-orphans;
    #docker image prune -f;
    ;;
  DUPD)
    rm -f .env;
    cp -fpvT .docker/.env.dev.dist .env;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . up -d --build --force-recreate --remove-orphans;
    #docker image prune -f;
    ;;
  QUPS)
    rm -f .env;
    cp -fpvT .docker/.env.qa.dist .env;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    cd src;
    rm -frd src/node_modules/;
    rm -frd src/dist/;
    sudo npm install;
    sudo npm install -g @angular/cli@8.1.3;
    sudo ng b --output-hashing=all --base-href /tramites-y-servicios/ --optimization --progress --skip-app-shell --delete-output-path --output-path ./dist/tramites-y-servicios/;
    cd ../;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . up --build;
    #docker-compose -f .docker/docker-compose_qa.yml --project-directory . up --build --force-recreate --remove-orphans;
    #docker image prune -f;
    ;;
  QUPD)
    rm -f .env;
    cp -fpvT .docker/.env.qa.dist .env;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    #docker-compose -f .docker/docker-compose_qa.yml --project-directory . up -d --build --force-recreate --remove-orphans;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . up -d --build --force-recreate;
    #docker image prune -f;
    ;;
  PUPS)
    rm -f .env;
    cp -fpvT .docker/.env.prod.dist .env;
    docker-compose -f .docker/docker-compose_prod.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_prod.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_prod.yml --project-directory . up --build --force-recreate --remove-orphans;
    #docker image prune -f;
    ;;
  PUPD)
    rm -f .env;
    cp -fpvT .docker/.env.prod.dist .env;
    docker-compose -f .docker/docker-compose_prod.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_prod.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_prod.yml --project-directory . up -d --build --force-recreate --remove-orphans;
    #docker image prune -f;
    ;;
  DBUILD)
    rm -f .env;
    cp -fpvT .docker/.env.dev.dist .env;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . build --compress --force-rm --pull;   
    #docker image prune -f;
    ;;
  QBUILD)
    rm -f .env;
    cp -fpvT .docker/.env.qa.dist .env;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . build --compress --force-rm --pull;   
    #docker image prune -f;
    ;;
  PBUILD)
    rm -f .env;
    cp -fpvT .docker/.env.prod.dist .env;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_qa.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_prod.yml --project-directory . build --compress --force-rm --pull;   
    #docker image prune -f;
    ;;
  DEBUG)
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . stop -t 0;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . rm -s -f -v; 
    #docker image prune -f;
    #docker container prune -f;
    docker-compose -f .docker/docker-compose_dev.yml --project-directory . DEBUG --compress --force-rm --pull;   
    #docker image prune -f;
    ;;
  *)
    echo -n "PAILA";
    ;;
esac

cat -AEn .env;
docker ps -sa;
docker container ps -sa;
ls -lathris;
